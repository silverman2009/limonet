const Telegraf = require('telegraf')
const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')
const users = require('./users');
var orders = require('./orders');

const bot = new Telegraf('599825944:AAHbBnm0ayHsC9i-Vb05o4N8_4fK0EI6F9M');

const redis = require("redis"), client = redis.createClient();

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017/test';

var khoy_r , tabriz_r , urmia_r;

MongoClient.connect(url, function(err, db){
    Userdb = db.collection('users');

    khoy_r	   = db.collection('khoy_resturant');
    tabriz_r   = db.collection('tariz_resturant');
    urmia_r	   = db.collection('urmia_resturant');
});

client.on("error", function (err) {
    console.log("Error " + err);
});

bot.start((ctx)=> {
  return ctx.reply('ربات دیلیم ویژه مدیریت رستوران ها', Extra.markup((markup) => {
    return markup.resize()
      .keyboard([
            markup.contactRequestButton('ورود')

      ])
  }))
});

bot.on("contact",(msg)=>{

    const cahtId = msg.chat.id;
    var phone = msg.update.message.contact.phone_number;

    if(phone.startsWith('98'))
        phone = phone.substr( 2 , phone.length - 2);
    else if (phone.startsWith('098'))
        phone = phone.replace("098" , "");
    else if (phone.startsWith("+98"))
        phone = phone.replace("+98" , "");
    else {}

    if (!phone.startsWith("0"))
        phone = "0"+ phone;


    if(phone =='09149608191'){
        client.set(phone+"_"+ "ChatId" ,cahtId);
        client.set(cahtId , phone);

        bot.telegram.sendMessage(cahtId, 'OK');
        return;
    }

    console.log("phone :: " +  phone);
    isRestaurantUserExist( phone , function (isExist) {

        var message ;
        if(isExist){

            client.set(phone+"_"+ "ChatId" ,cahtId);
            client.set(cahtId , phone);

            var RestaurantStatuse  ;

            message ='با تشکر از شما.'+ '\n' + 'از این پس پس از ارسال هر سفارش توسط مشتریان به صورت خودکار از طریق تلگرام نیز به شما اطلاع رسانی خواهد شد'
            //bot.telegram.sendMessage(cahtId, message);




            return msg.replyWithHTML(message , Extra.markup(
                Markup.keyboard(['سفارشات جدید', 'سفارشات تایید شده' , "تغییر وضغیت رستوران (باز و بسته بودن)"])
            ))

        }
        else {
            message= 'خطا!' + '\n' + 'این شماره عضو دیلیم نیست';
            bot.telegram.sendMessage(cahtId, message)
       }
    });
});

bot.hears('سفارشات جدید' , (ctx)=>{

    client.get(ctx.chat.id , (err , phone)=>{
        //console.log("Phone::::" + phone)
        //console.log("ctx.chat.id:::" + ctx.chat.id)

        getResturantNewOrderCollection( phone , (collection )=> {
              collection.find({"Orderinfo.Stause" : "Created"}).sort({"Orderinfo.FaktorNumber" : -1 }).limit(10).toArray( (err, orders) =>{

                  for (var i=0;i<orders.length;i++){
                      SendMessageToTelegram(phone ,orders[i] );
                  }
              })
        })
    })
});


bot.hears("سفارشات تایید شده" , (ctx)=>{
    client.get(ctx.chat.id , (err , phone)=>{
        getResturantNewOrderCollection( phone , (collection )=> {
            collection.find({"Orderinfo.Stause" : "preparing"}).sort({"Orderinfo.FaktorNumber" : -1 }).limit(10).toArray( (err, orders) =>{

                for (var i=0;i<orders.length;i++){
                    SendMessageToTelegram(phone ,orders[i] );
                }
            })
        })
    });
});


bot.hears("تغییر وضغیت رستوران (باز و بسته بودن)" , (ctx)=>{

    client.get(ctx.chat.id , (err , phone)=>{

        console.log(phone);

        getCityRestaurant(phone , (cr)=>{

            console.log("cr :::" + cr);

            cr.findOne({_id : phone+""} , (err , Restaurant)=>{
                if (!err){
                    if (Restaurant.isClosed){


                        return ctx.reply('<b>رستوران بسته است</b>  ', Extra.HTML().markup((m) =>
                            m.inlineKeyboard([
                                m.callbackButton('باز کردن' , "OpenRestaurant"),

                            ])))

                    }else
                        return ctx.reply('<b>رستوران باز است است</b>  ', Extra.HTML().markup((m) =>
                            m.inlineKeyboard([
                                m.callbackButton('بستن' , 'CloseRestaurant'),

                            ])))


                }


            })
        });
    })
});


function isRestaurantUserExist(phone , callback) {
     Userdb.findOne({phone : phone} , function (err , doc){
         if (doc != null) {

             if (doc.restuarant_user == null) {
                 callback(false);
             }else {
                 callback(true);
             }

         }else {
             callback(false);
         }

     });
}

var redCircle  =decodeURIComponent(escape('\xF0\x9F\x85\xBE'));
var add = decodeURIComponent(escape('\xf0\x9f\x93\x8d'));
var Money = decodeURIComponent(escape('\xf0\x9f\x92\xb0'));
var pirpileCircle = decodeURIComponent(escape('\xF0\x9F\x94\xB4'));
var OrangeDiamond = decodeURIComponent(escape('\xF0\x9F\x94\xB8'));

SendMessageToTelegram = function (phone  , order) {
    console.log(order);

    client.get(phone+"_"+ "ChatId" , function (err , id) {

        if (!err){

            var Message = "";


            var OrderStatuse ;

            if (order.Orderinfo.Stause =="Created")
                OrderStatuse = " جدید";
            else if (order.Orderinfo.Stause =="preparing")
                OrderStatuse = "تایید شده";
            else if (order.Orderinfo.Stause =="rejected")
                OrderStatuse = "رد شده";

            OrderStatuse = "سفارش" + " " + OrderStatuse +  "\n\n";


            Message +=  redCircle + " " + OrderStatuse  ;

            Message +="شماره تماس : " + "" + order.User.Phone + "\n";
            Message +="نام و نام خانوادگی : " + order.User.Name + "\n\n";


            for (var i = 0;i<order.Foods.length ; i++){

                var strFood = OrangeDiamond + " " + order.Foods[i].FoodName + "\n" +
                              OrangeDiamond + " " + order.Foods[i].qt + " " + "عدد";

                Message +=   strFood + "\n\n";
            }

            Message += "\n\n";

            Message += pirpileCircle + " " + "شماره فاکتور: " + order.Orderinfo.FaktorNumber + "\n" +
                 Money + " " + "جمع کل: " +numberWithCommas( order.Orderinfo.TotalPrice) + " " + "تومان" + "\n\n";


            if (!order.Orderinfo.IsAtPlace)
                Message += add + " " +  "آدرس: " + order.User.Address  + "\n";
            else
                Message +="نوع تحویل : تحویل در محل" + "\n" ;

            Message += "شماره موبایل: " + order.User.Phone + "\n\n";
            if ( order.Orderinfo.Description !="")
                Message += "توضیحات: " + order.Orderinfo.Description + "\n\n" ;

            try {

                var keyboard
                if (order.Orderinfo.Stause == "Created") {

                    keyboard= {
                        reply_markup: JSON.stringify({
                            inline_keyboard: [
                                [
                                    {
                                        text: 'تایید',
                                        callback_data: 'Accept' + " " + order.Orderinfo.FaktorNumber + " " + order.Resturant.ID + " " + order.User.Phone
                                    },
                                    {
                                        text: 'رد',
                                        callback_data: 'reject' + " " + order.Orderinfo.FaktorNumber + " " + order.Resturant.ID + " " + order.User.Phone
                                    }
                                ]
                            ]
                        })
                    };

                    bot.telegram.sendMessage(id, Message ,keyboard);
                }else if (order.Orderinfo.Stause == "preparing") {
                    keyboard= {
                        reply_markup: JSON.stringify({
                            inline_keyboard: [
                                [
                                    {
                                        text: 'ارسال سفارش',
                                        callback_data: 'sent' + " " + order.Orderinfo.FaktorNumber + " " + order.Resturant.ID + " " + order.User.Phone
                                    }
                                ]
                            ]
                        })
                    };
                    bot.telegram.sendMessage(id, Message ,keyboard);
                }
            }catch (ex){
                console.log("Err in bot.telegram.sendMessage");
                console.log(ex);
            }
        }
    })

};
//bot.telegram.sendMessage(29273890, 'hi')

const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

bot.on('callback_query', (msg) => {

    console.log(msg.update.callback_query)
    var strAction = msg.update.callback_query.data;

    var arr = strAction.split(' ');

    var FaktorNumber = arr[1] ,
        RestaurantPhone = arr[2] ,
        UserPhone =  arr[3];

    if (arr[0] =='Accept'){

        AcceptOrder( FaktorNumber ,  RestaurantPhone, UserPhone , (result)=>{
            if(result) {


                var keyboard = {
                    reply_markup: JSON.stringify({
                        inline_keyboard: [
                            [
                                {text:'ارسال سفارش', callback_data:'sent' + " " + FaktorNumber + " " + RestaurantPhone + " " + UserPhone  }

                            ]
                        ]
                    })
                };

                var chat_id    = msg.update.callback_query.message.chat.id;
                var message_id = msg.update.callback_query.message.message_id;

                console.log('chat_id:: ' + chat_id);
                console.log('message_id:: ' + message_id);

                bot.telegram.deleteMessage(msg.update.callback_query.message.chat.id , msg.update.callback_query.message.message_id)
                return msg.answerCbQuery('سفارش تایید شد');

            }
            else
                return  msg.answerCbQuery( 'خطا! لطفا مجددا سعی کنید');
        });

    }else if (arr[0] =='reject') {

        RejectOrder( FaktorNumber ,  RestaurantPhone, UserPhone, (result)=>{
            if(result) {
                bot.telegram.deleteMessage(msg.update.callback_query.message.chat.id , msg.update.callback_query.message.message_id)
                return msg.answerCbQuery('سفارش رد شد');

            }
            else
                return  msg.answerCbQuery( 'خطا! لطفا مجددا سعی کنید');
        });

    }else if (arr[0] =='sent') {

        sentOrder( FaktorNumber ,  RestaurantPhone, UserPhone, (result)=>{
            if(result) {
                bot.telegram.deleteMessage(msg.update.callback_query.message.chat.id , msg.update.callback_query.message.message_id)
                return msg.answerCbQuery('سفارش ارسال شد');
            }
            else
                return  msg.answerCbQuery( 'خطا! لطفا مجددا سعی کنید');
        });
    }else {
        if (strAction =="OpenRestaurant"){

            client.get(msg.chat.id , (err , phone)=>{

                getCityRestaurant(phone , (cr)=>{
                    cr.update({_id : phone} , {$set : {isClosed :false }} ,function (err , updated) {
                        if (updated)
                            return msg.answerCbQuery('رستوران باز شد');
                        else
                            return msg.answerCbQuery('خطا! لطفا مجددا سعی کنید');
                    })
                });
            });


        }else if (strAction == 'CloseRestaurant'){

            client.get(msg.chat.id , (err , phone)=>{

                getCityRestaurant(phone , (cr)=>{
                    cr.update({_id : phone} , {$set : {isClosed :true }} ,function (err , updated) {
                        if (updated)
                            return msg.answerCbQuery('رستوران بسته شد');
                        else
                            return msg.answerCbQuery('خطا! لطفا مجددا سعی کنید');
                    })
                });

            });

        }
    }
});

function getCityRestaurant(phone , callback) {
    Userdb.findOne({phone : phone+""   ,"restuarant_user" : true } ,  (err , Restaurant)=> {
        getCity_restaurant(Restaurant.City , (cr)=>{
            callback(cr);
        })
    });
}


function getCity_restaurant ( Citystr , callback){

    var city_rsturant ;

    if (Citystr == "khoy"){
        callback(khoy_r);
        console.log('khoy');
    }
    else if (Citystr == "tabriz"){
        callback(tabriz_r);
        console.log('tabriz');
    }
    else if (Citystr == "urmia"){
        callback( urmia_r);
        console.log('urmia');
    }
    else
        callback(null);
}

console.log("bot Started");

bot.startPolling();