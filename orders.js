var express = require('express');
var router = express.Router();
var moment = require('moment');
var jalaali = require('jalaali-js')

var redis = require("redis"), client = redis.createClient();

var ObjectID = require('mongodb').ObjectID;

var MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://127.0.0.1:27017/test';
var ResturantOrderDBURL = 'mongodb://127.0.0.1:27017/ResturantOrder';
var UserOrderDBURL = 'mongodb://127.0.0.1:27017/UserOrder';
let GiftCardDbUrl = 'mongodb://127.0.0.1:27017/GifCardsDB';

var bot = require('./bot');
var Cards = require('./Cards');

//Firebase
var admin = require('firebase-admin');
var DilimClient_admin  ;


var serviceAccount = require('./serviceAccountKey.json');
var DilimClient_serviceAccount = require('./DilimClient_serviceAccount.json');

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: 'https://limonet-5cfa8.firebaseio.com/'
    });

    DilimClient_admin =  admin.initializeApp({
        credential: admin.credential.cert(DilimClient_serviceAccount),
        databaseURL: 'https://dilimclient.firebaseio.com/'
    } , "DilimClient_admin");

}catch (ex){
    console.log('ERR')
    console.log(ex)
}

client.on("error", function (err) {
    console.log("Error " + err);
});

var resturants;
var Userdb ;

MongoClient.connect(url, function(err, db) {

    resturants = db.collection('resturant');
    Userdb = db.collection('users');

    khoy_r	   = db.collection('khoy_resturant');
    tabriz_r   = db.collection('tariz_resturant');
    urmia_r	   = db.collection('urmia_resturant');

});

var ResturantOrderDB ;
MongoClient.connect(ResturantOrderDBURL, function(err, db) {
    ResturantOrderDB = db;

    if(err){
        console.log(err);
    }
});

var UserOrderDB ;
MongoClient.connect(UserOrderDBURL, function(err, db) {
    UserOrderDB = db;

    if(err){
        console.log(err);
    }
});

let gifCards ;

MongoClient.connect(GiftCardDbUrl, function(err, db) {
    gifCards = db.collection('GiftCards');

    if(err){
        console.log(err);
    }
});

function sendMessage(phone , order , callback){

    order.User.Pass =null;

    var options ={
        priority : "high"
    };

    var payload = {

        data: {
            order: JSON.stringify(order)
        }
    };

    admin.messaging().sendToTopic( phone, payload , options).then(function(response){
        console.log("Successfully sent message:", response);
        setRestaurantSeenStatuse(phone  , false);
        SetSeenTimeOut(phone ,order.Orderinfo.FaktorNumber  );
        try {
            SendMessageToTelegram(phone, order);
            SendMessageToTelegram('09149608191', order);
            SendMessageToTelegram('09142036066', order);
        }catch (ex){}
        callback(true);
    }).catch(function(error) {
        console.log("Error sending message:", error);
        callback(false);
    });
}

function SendORderStatuseToUser( phone , faktNum , Statuse , callback) {

    var OrderStatuse = {
        OrderFaktorNumber : faktNum,
        OrderStatuse :  Statuse
    };

    var options ={
        priority : "high"
    };

    var payload = {

        data: {
            OrderStat: JSON.stringify(OrderStatuse)
        }
    };

    console.log("Phone ::" + phone);

    DilimClient_admin.messaging().sendToTopic( phone, payload , options).then(function(response){
        console.log(response);
        callback(true);
    }).catch(function(error) {
        console.log("Error sending message:", error);
        callback(false);
    });
}

function setRestaurantSeenStatuse(phone , isSeen) {
    client.set(phone+'_is_seen' , isSeen);
}


function IsUsedGiftCard(order) {
    return order.Dicount !=null;
}

router.get('/sendOrder/:vals', function(req, res, next) {

    var strOrder = decodeURIComponent( req.params.vals);


    try{
        console.log("Time :::  " +  moment.tz('Asia/Tehran').format('YYYY-MM-DD HH:mm:ss'));
    }catch(ex){}



    var order =JSON.parse(strOrder.replace(/\+/g , " "));

    var phone = order.User.Phone;
    var pass  = order.User.Pass;

    isLogedIn ( phone , pass , function (islogedIn) {
        if (!islogedIn){
            res.json({res : 401});
            res.end();
        }else {

            var data_time  =  getDate();
            console.log(data_time);
            order.Orderinfo.OrderDate =  new Date ((new Date).getTime() + 270*60*1000);  // (270*60*1000) -->  +04:30 ; Iran Time
            order.Orderinfo.Time = data_time.split(' ' )[1];
            order.Orderinfo.Stause ="Created";

            RestaurantID = order.Resturant.ID;



            getFaktorNo(function (FaktNo) {

                order.Orderinfo.FaktorNumber = FaktNo;
                order._id = FaktNo;

                AddNeworderToResturant(RestaurantID, order, function (isResturantOrderInserted) {
                    if (isResturantOrderInserted) {
                        AddNeworderToUser(order.User.Phone, order, function (isUserOrderInserted) {

                            if (isUserOrderInserted) {

                                SetorderTimeOut(FaktNo, order.Resturant.ID, order.User.Phone);

                                sendMessage(RestaurantID, order, function (isSented) {

                                    if (order.Orderinfo.isUsedFreeMoney){
                                        gifCards.findOne({_id: order.Dicount.DicountCode}, (err, GiftCard) => {
                                            GiftCard.IsUsed = true;
                                            GiftCard.isLocked = null;
                                            gifCards.save(GiftCard, {safe: true});

                                            var s = {};

                                            if (isSented) {
                                                s.res = 200;
                                                s.faktorNumber = FaktNo;
                                                res.end(JSON.stringify(s))
                                            } else {
                                                s.res = 500;
                                                res.end(JSON.stringify(s))
                                            }
                                        });
                                    } else {
                                        var s = {};

                                        if (isSented) {
                                            s.res = 200;
                                            s.faktorNumber = FaktNo;
                                            res.end(JSON.stringify(s))
                                        } else {
                                            s.res = 500;
                                            res.end(JSON.stringify(s))
                                        }
                                    }
                                });
                            }
                            else {
                                var s = {};
                                s.res = 500;
                                res.end(JSON.stringify(s))
                            }
                        });
                    }
                });
            });

        }
    });
});

var TimeOutTime = 10 * 60 * 1000;

function SetorderTimeOut(FaktorNumer , ResturantPhone , userPhone) {
    var d= setTimeout(function() {
        getResturantNewOrderCollection ( ResturantPhone , function(ResturantNewOrderCollection) {
            ResturantNewOrderCollection.findOne({_id: FaktorNumer * 1}, function (err, order) {
                if(order.Orderinfo.Stause == 'Created'){
                    RejectOrder ( FaktorNumer , ResturantPhone , userPhone , function (result ){
                    });
                }
            })
        });
    }, TimeOutTime);
}

var SeenTimeOutTime =2 * 60 * 1000;
function SetSeenTimeOut(phone , FaktorNum) {

    var d= setTimeout(function() {

        client.get (phone+'_is_seen' , function (err , isSeen) {
            if (isSeen)
                console.log(isSeen);
            else
                console.log(isSeen);


            getResturantNewOrderCollection ( phone , function(ResturantNewOrderCollection) {
                ResturantNewOrderCollection.findOne({_id: FaktorNum * 1}, function (err, order) {
                    if(order.Orderinfo.Stause == 'Created'){
                        senOrderToPhone(phone , FaktorNum);
                    }
                });
            });
        })

    }, SeenTimeOutTime);
}

router.get('/AcceptOrder/:ResturantPhone/:pass/:Phone/:FaktNum/:city/:Amount', function(req, res, next) {

    var _ = req.params;

    isLogedIn ( _.ResturantPhone , _.pass , function (isLogedIn){
        console.log('isLogedIn : ' + isLogedIn );

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }else{
            findReturantByPhone( _.ResturantPhone, _.city , function(resturant ){
                try{
                    AcceptOrder ( _.FaktNum ,resturant._id , _.Phone , function (result ){

                        /*if (parseInt(_.Amount) > 0){
                            var strDate = getStrDate();
                            client.incrby(_.ResturantPhone + "_" + strDate + "_us" , parseInt(_.Amount)* -1 );
                        }*/

                        var s 		=  {};
                        s.res 		=  200;
                        s.AcceptedDate = AcceptedDate;

                        res.end(JSON.stringify(s));
                    });
                }catch(ex) {
                    console.log(ex);
                }
            });
        }
    });
});

router.get('/RejectOrder/:ResturantPhone/:Phone/:pass/:FaktNum/:city/', function(req, res, next) {

    var _ = req.params;

    isLogedIn ( _.ResturantPhone  , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            findReturantByPhone( _.ResturantPhone, _.city , function(resturant ){ console.log()
                try{
                    RejectOrder ( _.FaktNum ,resturant._id , _.Phone , function (result ){
                        var s 		=  {};
                        s.res 		=  200;
                        res.end(JSON.stringify(s));
                    });
                }catch(ex) {
                    console.log(ex);
                }
            });
        }
    });
});

router.get('/sentOrder/:ResturantPhone/:pass/:Phone/:FaktNum/:city', function(req, res, next) {

    var _ = req.params;

    isLogedIn ( _.ResturantPhone , _.pass , function (isLogedIn){
        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            findReturantByPhone( _.ResturantPhone, _.city , function(resturant ){
                try{
                    sentOrder ( _.FaktNum ,resturant._id , _.Phone , function (result ){ console.log("is Sented : " +result )

                        var s 		=  {};
                        s.res 		=  200;
                        res.end(JSON.stringify(s));

                    });
                }catch(ex) {
                    console.log(ex);
                }
            });
        }
    });
});

router.get('/getRestaurantOrders/:Phone/:pass/:city', function(req, res, next) {


    var _ = req.params;

    setRestaurantSeenStatuse(_.Phone , true);
    //console.log(_);

    isLogedIn ( _.Phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            getResturantNewOrderCollection ( _.Phone ,  function (RestaurantNewOrder){
                RestaurantNewOrder.find({"Orderinfo.Stause" : {$ne : "sent"}}).sort({"Orderinfo.FaktorNumber" : -1 }).limit(90).toArray(function(err, orders) {
                    var s = {};
                    if (err){
                        console.log(err);
                        s.res = 500;
                        res.end(JSON.stringify(s));
                    }
                    else {
                        s.res  = 200;
                        s.orders = orders;
                        res.end(JSON.stringify(s));
                    }
                });
            });
        }
    });
});

router.get('/getRestaurantallOrders/:Phone/:pass/:city', function(req, res, next) {


    var _ = req.params;

    setRestaurantSeenStatuse(_.Phone , true);
    //console.log(_);

    isLogedIn ( _.Phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            getResturantNewOrderCollection ( _.Phone ,  function (RestaurantNewOrder){
                RestaurantNewOrder.find({"Orderinfo.Stause" : {$ne : "Created"}}).sort({"Orderinfo.FaktorNumber" : -1 }).limit(200).toArray(function(err, orders) {
                    var s = {};
                    if (err){
                        console.log(err);
                        s.res = 500;
                        res.end(JSON.stringify(s));
                    }
                    else {
                        s.res  = 200;
                        s.orders = orders;
                        res.end(JSON.stringify(s));
                    }
                });
            });
        }
    });
});

router.get('/getUserOrders/:Phone', function(req, res, next) {

    var _ = req.params;

    getLast5UserNewOrders ( _.Phone , function (orders){
        var s = {};

        s.res  = 200;
        s.orders = orders;
        res.end(JSON.stringify(s));

    });
});

router.get('/getOrderStatuse/:Phone/:FN', function(req, res, next) {

    var _ = req.params;
    getOrderStatuse(_.Phone , _.FN , function (stat){

        var s ={};
        s.res  = 200;
        s.stat = stat;

        res.end(JSON.stringify(s));
    });
});

//choise => net or total or us

router.get('/getRestaurantTatalSale/:Phone/:pass', function(req, res, next){

    var _ = req.params;
    var strDate = getStrDate();

    isLogedIn (_.Phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            client.multi().get(_.Phone + "_" + strDate + "_" + 'Total').get(_.Phone+ "_" + strDate + "_" + 'us').get(_.Phone+ "_" + strDate + "_" + 'net').exec(function (err, replies) {

                console.log('getRestaurantTatalSale');
                var s ={};
                s.res  = 200;

                s.Total = replies[0];
                s.us = replies[1];
                s.net = replies[2];

                //console.log(replies);
                res.end(JSON.stringify(s))
            });
        }
    });
});

router.get('/all', function(req, res, next) {

    var _ = req.params;
    var strDate = getStrDate();

    client.multi().get(_.Phone+ "_" + strDate + "_" + Total)
        .get(_.Phone+ "_" + strDate + "_" + us)
        .get(_.Phone+ "_" + strDate + "_" + net) .exec(function (err, replies) {
        console.log('replies 2 ');
        console.log(replies);
    });

});


router.get('/userOrders/:Phone/:hash', function(req, res, next) {

    var _ = req.params;

    isLogedIn(_.Phone , _.hash , function (isLogedIn) {
        if(isLogedIn){


            getUserNewOrderCollection ( _.Phone +"" , function (UserOrderCollection){
                UserOrderCollection.find ({ }).sort({"Orderinfo.FaktorNumber" : -1}) .toArray(function(err, orders) {

                    if(err){
                        res.json({res : 500});
                        res.end();
                    }else {
                        res.json({res : 200 , orders :orders })
                    }

                });
            });

        }else {
            res.json({res : 500});
            res.end();
        }
    });
});

function getDate(){
    var zone = 'Asia/Tehran';
    return moment.tz('Asia/Tehran').format('YYYY-MM-DD HH:mm:ss');
}

function getTime(){
    var zone = 'Asia/Tehran';
    return moment.tz('Asia/Tehran').format('HH:mm:ss');
}

function findReturantByPhone ( resturantPhone , city, callback){

    var city_rsturant ;//= khoy_r;

    if (city == "khoy")
        city_rsturant = khoy_r;
    else if (city == "tabriz")
        city_rsturant = tabriz_r;
    else if (city == "urmia")
        city_rsturant = urmia_r;
    else{}

    city_rsturant.findOne({_id  : resturantPhone},{Menues:0}  , function(err, docs) {
        callback(docs);
    });
}

function getFaktorNo(callback){
    client.incr('FaktorNo', function(err, FaktNo) {
        callback(FaktNo);
    });
}

getResturantNewOrderCollection = function (resturantPhone , callback){
    console.log('R_'+resturantPhone+'_New_Orders');

    var resturantOrderCollection =ResturantOrderDB.collection('R_'+resturantPhone+'_New_Orders');

    callback(resturantOrderCollection);
}

function AddNeworderToResturant (resturantPhone , order , callback){

    getResturantNewOrderCollection ( resturantPhone , function (NewOrderCollection  ){

        NewOrderCollection.insert ( order , function (err , doc ){
            if(!err)
                callback(true);
        });
    });
}

function getUserNewOrderCollection(phone , callback){
    var UserOrderCollection = UserOrderDB.collection('U_'+phone+'_New_Orders');

    callback(UserOrderCollection);
}

function getLast5UserNewOrders(phone , callback){
    getUserNewOrderCollection ( phone , function (coll){

        coll.find({} ).sort({_id: -1}).limit (5).toArray ( function (err , orders ){

            var orderInfos = [];

            for (var i=0;i< orders.length; i++){
                var order = {};

                order.faktNum = orders[i].Orderinfo.FaktorNumber;
                order.statuse = orders[i].Orderinfo.Stause;

                orderInfos.push(order);
            }

            callback(orderInfos);

        });
    });
}

function getOrderStatuse(phone , FaktorNumber , callback){  console.log(FaktorNumber)

    getUserNewOrderCollection ( phone , function (coll){
        //coll.find( {  "Orderinfo.FaktorNumber" : FaktorNumber} , function (err , order ){ console.log( order[0])

        coll.find( {"Orderinfo.FaktorNumber" :  parseInt (FaktorNumber)} ).toArray ( function (err , orders ){  //console.log(FaktorNumber+1)
            callback(orders[0].Orderinfo.Stause);
        });
    });
}

function AddNeworderToUser(phone , order , callback){
    getUserNewOrderCollection ( phone , function (NewOrderCollection){

        NewOrderCollection.insert ( order , function (err , doc ){
            if(!err)
                callback(true);
        });

    });
}

var AcceptedDate ;
AcceptOrder = function  ( faktorNumber ,ResturantPhone , UserPhone, callback){

    set_OrderStatuse_resturant ( ResturantPhone , faktorNumber ,"preparing" ,  function (IsresturantOrderAccepted){

        console.log("IsresturantOrderAccepted : " + IsresturantOrderAccepted);


        if (IsresturantOrderAccepted){

            Set_OrderStatuse_User ( UserPhone, faktorNumber , "preparing" , function (IsUserOrderAccepted){
                if (IsUserOrderAccepted) {

                    callback(true);
                }
                else
                    callback(false);
            });
        }
        else
            callback(false);
    });
}

RejectOrder = function  ( faktorNumber ,ResturantPhone , UserPhone , callback){

    set_OrderStatuse_resturant ( ResturantPhone , faktorNumber ,"rejected" ,  function (IsresturantOrderRejected){ console.log('IsresturantOrderRejected : '+ IsresturantOrderRejected)
        if (IsresturantOrderRejected){

            Set_OrderStatuse_User ( UserPhone, faktorNumber , "rejected" , function (IsUserOrderRejected){
                if (IsUserOrderRejected){
                    callback(true);
                }
                else
                    callback(false);
            });
        }
        else
            callback(false);
    });
}

sentOrder = function   ( faktorNumber ,ResturantPhone , UserPhone, callback){

    set_OrderStatuse_resturant ( ResturantPhone , faktorNumber ,"sent" ,  function (IsresturantOrdersent){
        if (IsresturantOrdersent){

            Set_OrderStatuse_User ( UserPhone, faktorNumber , "sent" , function (IsUserOrdersent){
                if (IsUserOrdersent)
                    callback(true);
                else
                    callback(false);
            });
        }
        else
            callback(false);
    });
}

function set_OrderStatuse_resturant( ResturantPhone  , faktorNumber , OederStatuse, callback){

    console.log( ResturantPhone + " " + faktorNumber);


    getResturantNewOrderCollection ( ResturantPhone , function(ResturantNewOrderCollection){

        ResturantNewOrderCollection.findOne ( {_id : faktorNumber*1} , function (err , order ){

            AcceptedDate = getDate();

            try {
                console.log("OederStatuse : " + OederStatuse);
                console.log("order.Orderinfo.Stause : " + order.Orderinfo.Stause);
            }catch (ex) {
                console.log("ERR in set_OrderStatuse_resturant:::" )
                console.log(ex);
            }
            if (OederStatuse ==order.Orderinfo.Stause)
                callback(false);
            else {
                order.Orderinfo.Stause = OederStatuse;
                order.Orderinfo.AcceptedDate = AcceptedDate;

                ResturantNewOrderCollection.save ( order , {safe: true} ,  function (err){
                    if (!err){
                        if ( OederStatuse =='preparing') {

                            if(order.Orderinfo.isUsedFreeMoney){
                                gifCards.findOne({_id :order.Dicount.DicountCode } , (err , card)=>{
                                    if (err)
                                        callback(false);
                                    else {
                                        Accounting(order , card , (isok)=>{
                                            if (isok)
                                                callback(true);
                                            else
                                                callback(false);
                                        } )
                                    }
                                });
                            }
                            else {
                                Accounting(order, null , function (isok) {
                                    if (isok)
                                        callback(true);
                                    else
                                        callback(false);
                                });
                            }

                        }else if( OederStatuse=='rejected'){

                            if(order.Orderinfo.isUsedFreeMoney) {
                                gifCards.findOne({_id: order.Dicount.DicountCode}, (err, card) => {
                                    card.IsUsed = false;
                                    card.isLocked = false;
                                    card.ForUser = null;

                                    gifCards.save( card , {safe :true} );
                                    callback(true);
                                });
                            }
                            else{
                                callback(true);
                            }
                        } else
                            callback(true);
                    }
                    else{
                        console.log(err);
                        callback(false);
                    }
                });
            }

        });
    });
}

function Accounting ( order , card , callback){

    var city = order.Resturant.city;
    console.log(order);

    getCity_restaurant (city , function (city_rsturant) {
        var OrderFoods = order.Foods;

        getFoods (city_rsturant , order.Resturant.ID , city  , function (Restaurantfoods){

            var TotalSale   = 0 ,
                portionOfUs = 0 ,
                netSale     = 0 ;


            for (var i=0;i<OrderFoods.length;i++){
                for (var j=0;j<Restaurantfoods.length;j++){
                    if(OrderFoods[i].Food_id == Restaurantfoods[j]._id)
                    {
                        var food = Restaurantfoods[j];


                        if (order.Orderinfo.isUsedFreeMoney){

                            if (order.Dicount.DiscountedFoodId == OrderFoods[i].Food_id){
                                if (order.Dicount.DiscountType == 'percent'){

                                    if (parseInt(card.PortationOfDilim) >0)
                                        portionOfUs -= parseInt(food.Price) * (parseInt(card.PortationOfDilim)/100);

                                    if (card.PortationOfRestaurant > 0)
                                        netSale -= parseInt(food.Price) * (parseInt(card.PortationOfRestaurant)/100);


                                    //netSale -= parseInt(food.Price) * ((parseInt(card.PortationOfDilim) + parseInt(card.PortationOfRestaurant  )/100)
                                }
                            }
                        }

                        if(!order.Orderinfo.isDiscounted) {

                            if(!order.ComeFromeQr)
                                portionOfUs += (parseInt(food.Price) - parseInt(food.PriceAfterDiscount)) * parseInt(OrderFoods[i].qt);
                            else {
                                portionOfUs += (parseInt(food.Price) - ( food.Price * ( (100-order.QR.Discount)/100 )) ) * parseInt(OrderFoods[i].qt)
                            }
                        }

                        if(!order.ComeFromeQr)
                            netSale 	+= parseInt(food.PriceAfterDiscount) * parseInt(OrderFoods[i].qt);
                        else
                            netSale 	+= (( food.Price * ( (100-order.QR.Discount)/100 )) ) * parseInt(OrderFoods[i].qt);
                    }
                }
            }

            var RestaurantID = order.Resturant.ID;
            var strDate = getStrDate();

            //client.incrby(RestaurantID + "_" + strDate + "_Total" , TotalSale);

            if (order.Orderinfo.isUsedFreeMoney)
                client.incrby(RestaurantID + "_" + strDate + "_Total" , order.Dicount.TotalAfterDicount);
            else
                client.incrby(RestaurantID + "_" + strDate + "_Total" , order.Orderinfo.TotalPrice);

            if(!order.isDiscounted)
                client.incrby(RestaurantID + "_" + strDate + "_us" , portionOfUs);
            client.incrby(RestaurantID + "_" + strDate + "_net" , netSale);

            //console.log("TotalSale : " + TotalSale);
            console.log("netSale : " + netSale);


            console.log("portionOfUs : " + portionOfUs);

            callback(true);

        });
    });
}

function getStrDate(){
    var date = jalaali.toJalaali(new Date());
    return date.jy + "_"+ date.jm;
}

function getFoods (city_rsturant , RestaurantID , city , callback){
    console.log("getFoods");
    city_rsturant.findOne ({"_id" : RestaurantID } , function(err, resaurant ) {
        console.log("getFoods");
        //console.log(resaurant.Menues);
        callback( resaurant.Menues);
    });
}

console.log("Orders.js");

function Set_OrderStatuse_User (userPhone ,faktorNumber , OederStatuse , callback){

    console.log('Set_OrderStatuse_User');

    getUserNewOrderCollection ( userPhone+"" , function (UserOrderCollection){
        UserOrderCollection.findOne ({_id :faktorNumber *1} , function (err ,order ){

            console.log('order : ')
            console.log(order);

            order.Orderinfo.AcceptedDate = AcceptedDate;
            order.Orderinfo.Stause = OederStatuse;

            UserOrderCollection.save ( order , {safe: true} ,  function (err){
                if (!err) {

                    SendORderStatuseToUser(userPhone ,faktorNumber , OederStatuse , (issent)=>{

                        if(issent)
                            callback(true);
                    });

                }
                else{
                    console.log('err IN Set_OrderStatuse_User');
                    callback(false);
                }
            });
        });
    });
}


function getCity_restaurant ( Citystr , callback){

    var city_rsturant ;

    if (Citystr == "khoy"){
        callback(khoy_r);
        console.log('khoy');
    }
    else if (Citystr == "tabriz"){
        callback(tabriz_r);
        console.log('tabriz');
    }
    else if (Citystr == "urmia"){
        callback( urmia_r);
        console.log('urmia');
    }
    else
        callback(null);
}


function isLogedIn(user , pass , callback) {

    Userdb.findOne({phone :user , Password :  pass} , function (err , user) {

        if(err)
            callback(false);
        else {
            callback( user == null ? false : true);
        }
    });
}


router.get('/sendOrder/:vals', function(req, res, next) {

    var strOrder = decodeURIComponent( req.params.vals);

    var order =JSON.parse(strOrder.replace(/\+/g , " "));

    var phone = order.User.Phone;
    var pass  = order.User.Pass;

    isLogedIn ( phone , pass , function (islogedIn) {
        if (!islogedIn){
            res.json({res : 401});
            res.end();
        }else {

            var data_time  =  getDate();
            console.log(data_time);
            order.Orderinfo.OrderDate =  new Date ((new Date).getTime() + 270*60*1000);  // (270*60*1000) -->  +04:30 ; Iran Time
            order.Orderinfo.Time = data_time.split(' ' )[1];
            order.Orderinfo.Stause ="Created";

            RestaurantID = order.Resturant.ID;

            getFaktorNo (function (FaktNo){

                order.Orderinfo.FaktorNumber = FaktNo;
                order._id = FaktNo;

                AddNeworderToResturant ( RestaurantID  , order , function(isResturantOrderInserted){
                    if(isResturantOrderInserted){
                        AddNeworderToUser ( order.User.Phone  , order , function (isUserOrderInserted){

                            if (isUserOrderInserted){

                                SetorderTimeOut(FaktNo , order.Resturant.ID , order.User.Phone );

                                sendMessage(RestaurantID ,order , function (isSented) {

                                    var s =  {};

                                    if (isSented){
                                        s.res 		=  200;
                                        s.faktorNumber = FaktNo;
                                        res.end(JSON.stringify(s))
                                    }else {
                                        s.res 		=  500;
                                        res.end(JSON.stringify(s))
                                    }

                                });
                            }
                            else{
                                var s 		=  {};
                                s.res 		=  500;
                                res.end(JSON.stringify(s))
                            }
                        });
                    }
                });
            });
        }
    });
});



router.get('/sendOrder_v2/:vals', function(req, res, next) {

    var strOrder = decodeURIComponent( req.params.vals);

    var order =JSON.parse(strOrder.replace(/\+/g , " "));

    var phone = order.User.Phone;
    var pass  = order.User.Pass;

    isLogedIn ( phone , pass , function (islogedIn) {
        if (!islogedIn){
            res.json({res : 401});
            res.end();
        }else {

            var data_time  =  getDate();
            console.log(data_time);
            order.Orderinfo.OrderDate =  new Date ((new Date).getTime() + 270*60*1000);  // (270*60*1000) -->  +04:30 ; Iran Time
            order.Orderinfo.Time = data_time.split(' ' )[1];
            order.Orderinfo.Stause ="Created";

            RestaurantID = order.Resturant.ID;


            console.log("order.Dicount == null ::: " + order.Dicount == null);

            if(order.Dicount == null){
                AddOrder(RestaurantID , order , res);
            }else{
                CheckLock(order.Dicount.DicountCode , order.User.Phone , (isLocked)=>{
                    console.log("isLocked :: "  + isLocked);
                    if(isLocked)
                        AddOrder(RestaurantID , order , res );
                    else{
                        res.json({res : 500 , Message : 'gift Card Lock Timed Out'});
                        res.end();
                    }
                });
            }
        }
    });
});

function AddOrder(_RestaurantID , order , res){

    getFaktorNo(function (FaktNo) {

        order.Orderinfo.FaktorNumber = FaktNo;
        order._id = FaktNo;

        AddNeworderToResturant(_RestaurantID, order, function (isResturantOrderInserted) {
            if (isResturantOrderInserted) {
                AddNeworderToUser(order.User.Phone, order, function (isUserOrderInserted) {

                    if (isUserOrderInserted) {

                        SetorderTimeOut(FaktNo, order.Resturant.ID, order.User.Phone);

                        sendMessage(RestaurantID, order, function (isSented) {

                            if (order.Orderinfo.isUsedFreeMoney) {
                                gifCards.findOne({_id: order.Dicount.DicountCode}, (err, GiftCard) => {
                                    GiftCard.IsUsed = true;
                                    GiftCard.isLocked = null;
                                    gifCards.save(GiftCard, {safe: true});

                                    var s = {};

                                    if (isSented) {
                                        s.res = 200;
                                        s.faktorNumber = FaktNo;
                                        res.end(JSON.stringify(s))
                                    } else {
                                        s.res = 500;
                                        res.end(JSON.stringify(s))
                                    }
                                });
                            } else {
                                var s = {};

                                if (isSented) {
                                    s.res = 200;
                                    s.faktorNumber = FaktNo;
                                    res.end(JSON.stringify(s))
                                } else {
                                    s.res = 500;
                                    res.end(JSON.stringify(s))
                                }
                            }
                        });
                    }
                    else {
                        var s = {};
                        s.res = 500;
                        res.end(JSON.stringify(s))
                    }
                });
            }
        });
    });
}

module.exports = router;