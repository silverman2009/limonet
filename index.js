var express  = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })
var fs = require('fs');
var redis = require("redis"), client = redis.createClient();
var ObjectID = require('mongodb').ObjectID;
var ejs = require('ejs')

var Jimp = require("jimp");

var db;
var Userdb ;

var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://127.0.0.1:27017/test';

var tags ;
var khoy_r , tabriz_r , urmia_r;

MongoClient.connect(url, function(err, db) {

    collection = db.collection('resturant');
    tags       = db.collection('Tags');

    khoy_r	   = db.collection('khoy_resturant');
    tabriz_r   = db.collection('tariz_resturant');
    urmia_r	   = db.collection('urmia_resturant');
    Userdb = db.collection('users');

});

router.get('/getAllResturant', function(req, res, next) {

    collection.find({"Active" : true},{Menues:0}).toArray(function(err, docs) {
        var s = {};
        s.res = docs;
        res.end(JSON.stringify(s));
    });
});

router.get('/getResturants/:city/:query/:phone', function(req, res, next) {
    var zone = 'Asia/Tehran';
    var _ = req.params;
    console.log("Phone : "+_.phone)
    console.log( moment.tz('Asia/Tehran').format('YYYY-MM-DD HH:mm:ss'));

    _query = decodeURIComponent(_.query)
    _.query =  _.query.replace( new RegExp('\\+', 'g'),' ')

    console.log(_.query )

    var city_rsturant ;//= khoy_r;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');

    }
    else
        res.end('Error');


    city_rsturant.find({tags : {$regex :_.query  } , "Active" : true},{Menues:0  , order:0}).sort({order : 1}).sort({"isClosed" : 1}).toArray(function(err, docs) {
        var s = {};
        s.res = docs;
        s.city = _.city;

        console.log(s);

        res.end(JSON.stringify(s));
    });
});

router.get('/getallResturants/:city/:phone', function(req, res, next) {
    var zone = 'Asia/Tehran';
    var _ = req.params;
    
    
    try{
        console.log("Time :::  " +  moment.tz('Asia/Tehran').format('YYYY-MM-DD HH:mm:ss'));
    }catch(ex){}


    var city_rsturant ;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');
    }
    else
        res.end('Error');


    city_rsturant.find({"Active" : true},{Menues:0 , order:0 }).sort({order : 1 , "isClosed" : 1}).toArray(function(err, docs) {

        var s = {};
        s.res = docs;
        s.city = _.city;

        res.json(s);
        res.end();
    });
});


router.get('/getallResturants_2/:city/:phone', function(req, res, next) {
    var zone = 'Asia/Tehran';
    var _ = req.params;


    var city_rsturant ;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');
    }
    else
        res.end('Error');


    //city_rsturant.find({},{Menues:0 , order:0 }).sort({order : 1}).toArray(function(err, docs) {
    city_rsturant.find({"Active" : true},{Menues:0 , order:0 }).sort({order : 1 , "isClosed" : 1}).toArray(function(err, docs) {
        var s = {};
        s.res = docs;
        s.city = _.city;

        res.json(s);
        res.end();
    });
});



router.get('/getallResturantsMenue/:City', function(req, res, next) {

    var _ = req.params;
    getCity_restaurant(_.City , (KhoyRestaurant)=>{

        KhoyRestaurant.find( {}).sort({order : 1}).toArray((err , Restaurants)=>{

            if (err) {
                res.end('Err');
                return;
            }



            var strMenues =  "";

            for(var i=0;i<Restaurants.length;i++){

                var r = Restaurants[i];

                strMenues += ( r.Name) + "</br>";

                var strFoods ="";

                for(var j =0;j<r.Menues.length;j++){
                    var food  = r.Menues[j];

                    strFoods += food.Name + " " + food.Price + " " + food.Discount +  "</br>"
                }

                strMenues += strFoods;

                strMenues +="</br></br>"

            }

            res.set('Content-Type', 'text/html');
            res.end(strMenues)
        })


    });


});

router.get('/getRestaurantDeliveryPrice/:city/:phone', function(req, res, next) {

    var _ = req.params;
    var phone =  _.phone;

    var city_rsturant ;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');

    }
    else
        res.end('Error');

    city_rsturant.find({ _id : phone },{DeliveryPrice:1 }).toArray(function(err, docs) {
        var s = {};
        s.res = 200;
        s.DeliveryPrice =  docs[0].DeliveryPrice
        res.end(JSON.stringify(s));
    });

});

router.get('/getRestaurantWorkingTime/:city/:phone', function(req, res, next) {

    var _ = req.params;
    var phone =  _.phone;

    var city_rsturant ;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');
    }
    else
        res.end('Error');

    city_rsturant.find({ _id : phone } ).toArray(function(err, docs) {
        var s = {};
        s.res = 200;
        s.WorkingTime_End =  docs[0].WorkingTime_End;
        s.WorkingTime_Start = docs[0].WorkingTime_Staart;
        res.end(JSON.stringify(s));
    });

});

router.post('/getResturantMenue', function(req, res, next) {

    var _ = req.body;

    var city_rsturant ;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');
    }
    else
        res.end('Error');

    city_rsturant.find({_id:  _.id}).toArray(function(err, docs) {
        var s = {};
        s.res = docs;
        res.end(JSON.stringify(s));
    });
});

router.post('/updateFood', upload.single('image'), function (req, res, next) {

    var _ = req.body;
    var photoName ;

    isLogedIn ( _.Phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            if(req.file!=null)
                photoName = req.body.RestaurantId + "_" + _.FoodId +"."+ req.file.originalname.split(".")[1] ;

            getCity_restaurant(_.city  , function (city_restaurant){

                city_restaurant.findOne ({_id : _.Phone }, function (err ,restaurant ){

                    for (var i = 0;i<restaurant.Menues.length;i++){
                        if (restaurant.Menues[i]._id == parseInt(_.FoodId)){

                            var food = restaurant.Menues[i];
                            food.isActive = _.FoodStatuse =="1" ? true : false;
                            food.Name = _.Name;
                            food.Price = _.Price;
                            food.Description =  _.Description;
                            food.category = _.category;
                            //food.Discount =  _.Discount;
                            food.PriceAfterDiscount = _.PriceAfterDiscount;
                            if(req.file!=null)
                                food.Photo = photoName;

                            city_restaurant.save(restaurant , {safe: true});


                            if(req.file!=null){
                                var tmp_path = req.file.path;

                                var target_path = '/home/silverman/Yemali/public/FoodPic/' + photoName;

                                var src = fs.createReadStream(tmp_path);
                                var dest = fs.createWriteStream(target_path);
                                src.pipe(dest);

                                src.on('end', function() {
                                    console.log('end....................')  ; res.json({'response':"Saved"}); res.end();}
                                );

                                src.on('error', function(err) {
                                    res.json({'response':"Error"});
                                    res.end();
                                });
                            }else {
                                res.json({'response':"Saved"})
                                res.end();
                            }
                        }
                    }
                });
            });
        }
    });
});

router.get('/rename', function(req, res, next) {
    /*
        khoy_r.findOne({_id:  '77777777777' } , function(err, restaurant) {

            for (var i = 0;i<restaurant.Menues.length;i++){
                if(restaurant.Menues[i].category =="???")
                    restaurant.Menues[i].category = "???????"
            }

            khoy_r.save(restaurant , {safe: true});
        });
    */
});

router.post('/deleteFood', function(req, res, next) {

    var _ = req.body;

    var RestaurantId = _.RestaurantId;
    var FoodId = _.FoodId;
    var pass = _.password;


    isLogedIn ( RestaurantId , pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{

            var city_rsturant ;
            if (_.city == "khoy"){
                city_rsturant = khoy_r;
                console.log('khoy');
            }
            else if (_.city == "tabriz"){
                city_rsturant = tabriz_r;
                console.log('tabriz');
            }
            else if (_.city == "urmia"){
                city_rsturant = urmia_r;
                console.log('urmia');
            }else
                res.end('Error');


            city_rsturant.update({_id : RestaurantId } , {$pull : { "Menues" : { "_id" : parseInt(FoodId) } } } , function (err , result){
                var s = {};

                if (err){
                    console.log(err);
                    s.res = 500;
                    res.end(JSON.stringify(s));
                }
                else
                {
                    s.res = 200;
                    res.end(JSON.stringify(s));
                }
            });
        }
    });

});

router.post('/addRestaurant', function(req, res, next) {

    var _ = req.body;
    var phone = _.phone;

    var city_rsturant ;//= khoy_r;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;
        console.log('khoy');
    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
        console.log('tabriz');
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
        console.log('urmia');
    }
    else
        res.end('Error');

    var _tags ;
    _tags = _.Tags.replace( new RegExp('\\+', 'g'),' ').split("،");

    var _Areas = _.Areas.replace( new RegExp('\\+', 'g'),' ').split("،");

    insertRestaurant (_ , city_rsturant , function (isInserted){

        var s ={};
        if(isInserted){
            s.res = 200;
        }else
            s.res = 500;

        res.end(JSON.stringify(s));

    });
});


router.post('/EditRestaurantProfile', function(req, res, next) {

    var _ = req.body;
    var phone = _.phone;
    console.log(_)
    var city_rsturant ;

    if (_.city == "khoy"){
        city_rsturant = khoy_r;

    }
    else if (_.city == "tabriz"){
        city_rsturant = tabriz_r;
    }
    else if (_.city == "urmia"){
        city_rsturant = urmia_r;
    }
    else
        res.end('Error');

    var _tags ;
    _tags = decodeURIComponent( _.Tags).replace( new RegExp('\\+', 'g'),' ').split("،");

    var _Areas = decodeURIComponent(_.Areas).replace( new RegExp('\\+', 'g'),' ').split("،");

    EditRestaurant (_ , city_rsturant , function (isInserted){

        var s ={};
        if(isInserted){
            s.res = 200;
        }else
            s.res = 500;

        res.end(JSON.stringify(s));

    });
});

router.get('/isRestaurantProfileExist/:city/:phone/:pass', function(req, res, next) {
    var _ = req.params;

    isLogedIn ( _.phone , _.pass , function (isLogedIn){


        if( !isLogedIn){
            var s = {};
            s.res = 401;
            res.end(JSON.stringify(s));
        }
        else{
            getCity_restaurant(req.params.city  , function (city_restaurant){

                var s ={};

                if(city_restaurant !=null){

                    city_restaurant.findOne({_id : _.phone} , function (err , restaurant){

                        if (err !=null | restaurant == null){
                            s.res = 500;
                            res.end(JSON.stringify(s));
                        }
                        else{
                            if (restaurant.WorkingTime_End == null){
                                s.res = 500;
                                res.end(JSON.stringify(s))
                            }

                            else{
                                s.res = 200;
                                res.end(JSON.stringify(s));
                            }
                        }
                    });
                }
                else{
                    s.res = 500;
                    res.end(JSON.stringify(s));
                }
            });
        }
    });
});

router.get( '/getrestaurantProfile/:city/:phone/:pass' , function(req, res, next) {
    var _ = req.params;

    isLogedIn ( _.phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            getCity_restaurant(req.params.city  , function (city_restaurant){


                var s ={};

                if(city_restaurant !=null){

                    city_restaurant.findOne({_id : _.phone}  , {Menues : 0}, function (err , restaurant){
                        if (err !=null ){
                            s.res = 500;
                            res.end(JSON.stringify(s));
                        }else{
                            s.res = 200;
                            s.restaurant = restaurant;
                            res.end(JSON.stringify(s));
                        }
                    });
                }
                else{
                    s.res = 500;
                    res.end(JSON.stringify(s));
                }
            });
        }
    });
});

router.get('/getrestaurantFoodCategories/:city/:phone' , function(req, res, next) {

    getCity_restaurant(req.params.city  , function (city_restaurant){

        var _ = req.params;
        var s ={};

        if(city_restaurant !=null){
            city_restaurant.findOne({_id : _.phone}  , {Menues : 0}, function (err , restaurant){ console.log(restaurant)
                s.res = 200;
                s.FoodCategories = restaurant.FoodCategories;
                res.end(JSON.stringify(s));
            });
        }else{
            s.res = 500;
            res.end(JSON.stringify(s));
        }
    });
});

router.post('/addFoodToMenue', function(req, res, next) { // deprecated

    var _ = req.body;

    var s;
    var food = {};

    food.Name = _.Name ;
    food.Price = _.Price ;
    food.Description = _.Description;
    food.Type =0;
    food.category = _.category;
    food.Discount = _.Discount;
    food.PriceAfterDiscount = _.PriceAfterDiscount;
    food.Photo="";

    getCity_restaurant(_.city  , function (city_restaurant){
        city_restaurant.findOne ({_id : _.Phone} , function (err , _r){

            var _id =  (_r.Menues.length) + 1;
            food._id = _id;

            city_restaurant.findOneAndUpdate({_id : _.Phone}  , {$push : {Menues : food} }, function (err , restaurant){

                if (err){
                    console.log(err);
                    s={};
                    s.res = 500;
                    res.end(JSON.stringify(s));
                }else{
                    s={};
                    s.res = 200;
                    s.FoodId = _id;
                    res.end(JSON.stringify(s));
                }
            });
        });
    })
});

router.get( '/setCategories/:dd' , function(req, res, next) {


    console.log(req.params.dd);

    var _ = JSON.parse(req.params.dd.replace(/\+/g , " "));
    isLogedIn ( _.phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{

            getCity_restaurant(_.city  , function (city_restaurant){

                var categories = [];

                for (var i=0;i< _.Cats.length ; i++){
                    var category = {};
                    category.order = i;
                    category.Name =  JSON.parse(_.Cats[i] ).cat;
                    categories.push(category)
                }

                //console.log('categories');
                //console.log(categories);
                //console.log(_.phone);

                city_restaurant.findOneAndUpdate({_id : _.phone}  , {$set : {FoodCategories :categories } } , function (err , result ){

                    var s={};
                    if(err){
                        s.res = 500;
                        res.end(JSON.stringify(s));
                    }else{
                        s.res = 200;
                        res.end(JSON.stringify(s));
                    }
                });
            });
        }
    });
});




function insertRestaurant(_ , city_rsturant  , callback){

    var _Areas = decodeURIComponent(_.Areas.replace( new RegExp('\\+', 'g'),' ')).split("،");
    var phone = _.phone;

    var _tags  = [];
    _tags.push (decodeURIComponent(_.ResturantName.replace( new RegExp('\\+', 'g'),' ')) );

    city_rsturant.findOneAndUpdate ({_id : phone } , {$set : {
            Phone : _.RsturantPhone ,
            Name : decodeURI(_.ResturantName.replace( new RegExp('\\+', 'g'),' ')) ,
            Description :decodeURI( _.Description.replace( new RegExp('\\+', 'g'),' ')),
            DeliveryPrice : _.DeliveryPrice ,
            MinimumOrderAmount : _.MinimumDeliveryAmount ,
            tags : _tags ,
            Areas : _Areas ,
            WorkingTime_Staart : _.StartTime ,
            WorkingTime_End : _.EndTime  ,
            Address : decodeURIComponent(_.Address.replace( new RegExp('\\+', 'g'),' ')),
            Active : false , Menues : [], Photo : "" , Type : 0  }} ,  function (err , result){
        if(err){
            console.log(err);
            callback(false);
        }
        else
            callback(true);
    });
}

function EditRestaurant(_ , city_rsturant  , callback){

    var _Areas = decodeURIComponent(_.Areas.replace( new RegExp('\\+', 'g'),' ')).split("،");
    var phone = _.phone;

    var _tags  = [];
    _tags.push (decodeURIComponent(_.ResturantName.replace( new RegExp('\\+', 'g'),' ')) );

    city_rsturant.findOneAndUpdate ({_id : phone } , {$set : {

            Name : decodeURI(_.ResturantName.replace( new RegExp('\\+', 'g'),' ')) ,
            Description :decodeURI( _.Description.replace( new RegExp('\\+', 'g'),' ')),
            DeliveryPrice : _.DeliveryPrice ,
            MinimumOrderAmount : _.MinimumDeliveryAmount ,
            tags : _tags ,
            Areas : _Areas ,
            WorkingTime_Staart : _.StartTime ,
            WorkingTime_End : _.EndTime  ,
            Address : decodeURI(_.Address.replace( new RegExp('\\+', 'g'),' '))  }} ,  function (err , result){
        if(err){
            console.log(err);
            callback(false);
        }
        else
            callback(true);
    });
}

router.get('/CityTags/:_city', function(req, res, next) {

    console.log(req.params._city);
    tags.find({city : req.params._city},{tags : 1}).toArray(function(err, docs) {
        var s = {};
        s.res = docs;
        res.end(JSON.stringify(s));
    });
});

router.get('/setCityTags/:_city', function(req, res, next) {


    getCity_restaurant (req.params._city , function (db){
        if(db == null) console.log("Nulllll");

        db.find({"Active" : true},{tags:1}).toArray (function (err , docs){


            var T = [];
            for(var i =0;i<docs.length;i++){

                if(docs[i].tags != null) { console.log(docs);

                    for (var j = 0; j < docs[i].tags.length; j++){ console.log(docs[i].tags[j]);
                        if ( T.indexOf(docs[i].tags[j]) == -1 ) // Not Repetive Tag
                            T.push(docs[i].tags[j]);
                    }
                }
            }

            tags.update({city : req.params._city} , {$set : {tags : T}})
            var s = {};
            s.tags = T;
            res.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
            res.end(JSON.stringify(s));

        });
    });
});


router.post('/updateFood_v2', upload.single('image'),(req, res)=>{

    let _ = req.body;
    let photoName ;

    isLogedIn ( _.Phone , _.pass , function (isLogedIn){

        if( !isLogedIn){
            let s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            if(req.file!=null)
                photoName = req.body.RestaurantId + "_" + _.FoodId +"."+ req.file.originalname.split(".")[1] ;

            getCity_restaurant(_.city  , function (city_restaurant){

                city_restaurant.findOne ({_id : _.Phone }, function (err ,restaurant ){

                    for (let i = 0;i<restaurant.Menues.length;i++){
                        if (restaurant.Menues[i]._id === parseInt(_.FoodId)){

                            let food = restaurant.Menues[i];
                            food.isActive = _.FoodStatuse ==="1" ;
                            food.Name = _.Name;
                            food.Price = _.Price;
                            food.Description =  _.Description;
                            food.category = _.category;
                            food.Discount =  _.Discount;
                            food.PriceAfterDiscount = _.PriceAfterDiscount;
                            if(req.file!=null)
                                food.Photo = photoName;

                            city_restaurant.save(restaurant , {safe: true});


                            if(req.file!=null){
                                let tmp_path = req.file.path;

                                let target_path = '/home/silverman/Yemali/public/FoodPic/' + photoName;

                                let src = fs.createReadStream(tmp_path);
                                let dest = fs.createWriteStream(target_path);
                                src.pipe(dest);

                                src.on('end', function() {
                                    console.log('end....................') ;
                                    res.json({'response':"Saved"});
                                    res.end();}
                                );

                                src.on('error', function(err) {
                                    console.log("Err in Upload Photo:::");
                                    console.log(err);

                                    res.json({'response':"Error"});
                                    res.end();
                                });
                            }else {
                                res.json({'response':"Saved"});
                                res.end();
                            }
                        }
                    }
                });
            });
        }
    });
});

router.post('/uploads', upload.single('image'), function (req, res, next) {

    isLogedIn ( req.body.Phone , req.body.pass  , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            AddFodToMenue(req.body , req , function (s) {

                if(s.res = 200){

                    var tmp_path = req.file.path;

                    var photoName = req.body.RestaurantId + "_" + s.FoodId +"."+ req.file.originalname.split(".")[1] ;

                    var target_path = '/home/silverman/Yemali/public/FoodPic/' + photoName;

                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);

                    src.on('end', function() {
                        console.log('end....................');
                        res.json({'response':"Saved"});
                        res.end();}
                    );

                    src.on('error', function(err) {
                        res.json({'response':"Error"});
                        res.end();
                    });
                }
                else {
                    res.json({'response':"Error"});
                    res.end();
                }
            })
        }
    });
});

router.post('/upload_RestaurantPhoto', upload.single('image'), function (req, res, next) {

    var _ = req.body;

    isLogedIn ( _.Phone ,_.pass , function (isLogedIn){

        if( !isLogedIn){
            var s = {};
            s.code = 401;
            res.end(JSON.stringify(s));
        }
        else{
            SetRestaurantPhoto (_.RestaurantId , _.city ,req.file , function (result , photoName){

                if(result){

                    var tmp_path = req.file.path;
                    var target_path = '/home/silverman/Yemali/public/RestaurantPhotos/' + photoName;

                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);

                    src.on('end', function() {
                        console.log('end....................');
                        res.json({'response':"Saved"});
                        res.end();
                    });
                    src.on('error', function(err) {
                        res.json({'response':"Error"});
                        res.end();
                    });
                }else {
                    res.json({'response':"Error"});
                    res.end();
                }
            });
        }
    });
});

function getFoodId(callback){
    client.incr('FoodId', function(err, FaktNo) {
        callback(FaktNo);
    });
}

function SetRestaurantPhoto( RestaurantId , City , photo , callback) {

    var  photoName = RestaurantId + "." + photo.originalname.split(".")[1];
    console.log(photoName);
    getCity_restaurant( City  , function (city_restaurant){

        city_restaurant.findOneAndUpdate({_id :  RestaurantId} , {$set : { Photo : photoName }} , function (err , result) {
            if(err)
                callback(false , null);
            else
                callback(true , photoName);


        });
    });
}

router.get('/EditCategoryName/:RestaurantId/:city/:OldName/:NewName', function(req, res, next) {
    var _ = req.params;

    editCategoryName( _.RestaurantId , _.city , _.OldName , _.NewName , function (resault) {

        if (resault){
            res.json({res : 200});
            res.end();
        }else{
            res. json({res : 500});
            res.end();
        }
    })
});

function editCategoryName( RestaurantId , city , OldName , NewName , callback ) {

    getCity_restaurant( city  , function (city_restaurant){
        city_restaurant.findOne({_id :RestaurantId} , function(err, Restaurant) {

            //var menues = Restaurant.Menues;
            //var FoodCategories = Restaurant.FoodCategories;

            if (err)
                callback(false);

            for (var i =0;i<Restaurant.Menues.length;i++){
                if (Restaurant.Menues[i].category == OldName)
                    Restaurant.Menues[i].category = NewName;
            }


            for (var i=0;i<Restaurant.FoodCategories.length;i++){
                if (Restaurant.FoodCategories[i].Name == OldName)
                    Restaurant.FoodCategories[i].Name = NewName;
            }

            city_restaurant.save( Restaurant , {safe: true});

            callback(true)
        });
    });

}

function AddFodToMenue(_  , req ,callback ) {

    var s;
    var food = {};

    getCity_restaurant(_.city  , function (city_restaurant){
        getFoodId( function ( _id){

            //var _id =  (_r.Menues.length) + 1;
            food._id = _id;

            food.Name = _.Name ;
            food.Price = _.Price ;
            food.Description = _.Description;
            food.Type =0;
            food.category = _.category;
            food.Discount = _.Discount;
            food.PriceAfterDiscount = _.PriceAfterDiscount;
            food.Photo = _.RestaurantId + "_" + _id +"." + req.file.originalname.split(".")[1]  ;

            console.log(_.RestaurantId + "_" + _id +"." + req.file.originalname.split(".")[1])

            city_restaurant.findOneAndUpdate({_id : _.Phone}  , {$push : {Menues : food} }, function (err , restaurant){

                if (err){
                    console.log(err)
                    s={};
                    s.res = 500;
                    //res.end(JSON.stringify(s));
                    callback(s);
                }else{
                    s={};
                    s.res = 200;
                    s.FoodId = _id;
                    //res.end(JSON.stringify(s));
                    callback(s);
                }
            });

        });
    })
}

function getCity_restaurant ( Citystr , callback){

    var city_rsturant ;

    if (Citystr == "khoy"){
        callback(khoy_r);
        console.log('khoy');
    }
    else if (Citystr == "tabriz"){
        callback(tabriz_r);
        console.log('tabriz');
    }
    else if (Citystr == "urmia"){
        callback( urmia_r);
        console.log('urmia');
    }
    else
        callback(null);
}

function isLogedIn(user , pass , callback) {

    Userdb.findOne({phone :user , Password :  pass} , function (err , user) {

        if(err)
            callback(false);
        else {
            callback( user == null ? false : true);
        }
    });
}


router.get('/CompressImage/:ImageName', function(req, res, next) {
    var _ = req.params;

    let imageSrc = '/home/silverman/Yemali/public/FoodPic/' + _.ImageName;

    Jimp.read(imageSrc).then(function (lenna) {
        lenna.resize(400, 300)
            .quality(70)
            .write(imageSrc);
        res.json({res : 200})
        res.end();

    }).catch(function (err) {
        console.error(err);
    });
});


router.get('/getallResturants_3/:city/:phone', function(req, res) {
    //let zone = 'Asia/Tehran';
    let _ = req.params;

    getCity_restaurant(_.city ,(city_rsturant)=>{
        if (city_rsturant==null)
            res.end("Error");
        else{
            city_rsturant.find({},{Menues:0 , order:0 }).sort({order : 1}).toArray(function(err, docs) {
            //city_rsturant.find({"Active" : true},{Menues:0 , order:0 }).sort({order : 1 , "isClosed" : 1}).toArray(function(err, docs) {
                let s = {};
                s.res = docs;
                s.city = _.city;

                res.json(s);
                res.end();
            });
        }
    });
});

module.exports = router;