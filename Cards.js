var express = require('express');
var router = express.Router();
var moment = require('moment');

var MongoClient = require('mongodb').MongoClient;
var GiftCardsDBurl = 'mongodb://127.0.0.1:27017/GifCardsDB';

var GiftCards_col ;



MongoClient.connect(GiftCardsDBurl, function(err, db) {

    GiftCards_col = db.collection('GiftCards')

});

router.get('/CreatePCard/:id/:amount/:expDate/:restaurantId/:restaurantName/:foodName/:foodId/:for_User/:description', function(req, res, next) {  // Percentage Gift Cards

    let _ = req.params;

    let GiftCard = {    _id : _.id  ,
                        Type : 'Discount',
                        Amount : _.amount ,
                        ExpDate : new Date(_.expDate) ,
                        RestaurantId : _.restaurantId ,
                        FoodId :_.foodId ,
                        FoodName : _.foodName,
                        ForUser :  _.for_User ,
                        RestaurantName : _.restaurantName,
                        IsUsed : false,
                        Description  :_.description};


    GiftCards_col.insert(GiftCard , function (err , inserted) {

        if (inserted){
            res.json({res : 200});
            res.end();
        }else {
            res.json({res : 500});
            res.end();
        }
    })
});

router.get('/check/:Cardid/:Restaurant_id/:userPhone' , function(req, res, next){

    let _ = req.params;

    GiftCards_col.findOne({_id : _.Cardid}  , (err , card)=>{

        if(err){
            res.json({res : 501});
            res.end();
        }else {
            if (card == null){
                res.json({res : 501 , Message: 'Card Not Found'});
                res.end();
            }else {
                if (card.IsUsed){
                    res.json({res : 501 , Message : 'Already Used' });
                    res.end();

                } else if (card.RestaurantId !=_.Restaurant_id){
                    res.json({res : 500 , ForRestaurant : card.RestaurantName });
                    res.end();
                }else if(card.ExpDate < new Date(moment().format('YYYY-MM-DD')) ){
                    res.json({res : 501 , Message : 'Expired'});
                    res.end();
                }
                else {
                    /*card.IsUsed = true;

                    GiftCards_col.save(card , {safe: true} , function (err , updated) {
                        if (updated){
                            res.json({res : 200 , Amount : card.Amount , Type : card.Type , FoodId :card.FoodId  , FoodName : card.FoodName});
                            res.end();
                        }else {
                            res.json({res : 501 , Message : 'Not Updated'});
                            res.end();
                        }
                    })*/

                    LockCard(card._id ,_.userPhone, (isLocked)=>{
                        if(isLocked){
                            res.json({res : 200 , Amount : card.Amount , Type : card.Type , FoodId :card.FoodId  , FoodName : card.FoodName});
                            res.end();
                        }else{
                            res.json({res : 500});
                            res.end();
                        }

                    })
                }
            }
        }
    })
});


router.get('/unlockCard/:Cardid' , function(req, res, next){
    let _ = req.params;

    getcard(_.Cardid , (card)=>{
        card.isLocked = false;
        card.IsUsed = false;
        GiftCards_col.save(card , {safe : true});

        res.json({res : 200});
        res.end();
    });

});

router.post('/CreateGiftCard' , (req , res)=>{
    console.log(req.body);

    let _ = req.body;

    let GiftCard = {
        _id : _.id  ,
        Type : 'Discount',
        Amount : _.amount ,
        ExpDate : new Date(_.expDate) ,
        RestaurantId : _.restaurantId ,
        FoodId :_.foodId ,
        FoodName : _.foodName,
        ForUser :  _.for_User ,
        RestaurantName : _.restaurantName,
        IsUsed : false,
        Description  :_.description ,
        PortationOfDilim : _.portationOfDilim ,
        PortationOfRestaurant :_.PortationOfRestaurant };


    GiftCards_col.insert(GiftCard , function (err , inserted) {

        if (inserted){
            res.json({res : 200});
            res.end();
        }else {
            res.json({res : 500});
            res.end();
        }
    })
});

router.get('/getCardDescriptions/:Cardid' , function(req, res, next) {

    let _ = req.params;

    GiftCards_col.findOne({_id : _.Cardid}  , (err , card)=>{ console.log("card") ; console.log(card)
        if (card ==null){
            res.json({res : 501});
            res.end();
        }else {
            if (card==null){
                res.json({res : 404});
                res.end();
            }else {
                res.json({res : 200 , Description : card.Description});
                res.end();
            }
        }
    })
});

LockCard = function (cardId , forUser, callback) {

    getcard(cardId , (card)=>{

        if(card == null)
            callback(false);
        else {
            if(card.isLocked !=true) {
                card.isLocked = true;
                card.IsUsed = true;
                card.ForUser = forUser;

                GiftCards_col.save(card, {safe: true});
                unLockIfNotUsed(cardId );
                callback(true);

            }else {
                callback(false);
            }
        }
    });
};

CheckLock = function (cardId , UserID , callback) {
    getcard(cardId , (card)=>{
        if (card.ForUser == UserID )
            callback(true);
        else
            callback(false);
    });
};

getcard = function (cardId , callback) {

    GiftCards_col.findOne({_id : cardId}  , (err , card)=>{
      if (err)
          callback(null);
      else
          callback(card)
    });
};

//var TimeOutTime = 2 * 60 * 1000;

var TimeOutTime =60 * 1000;

unLockIfNotUsed = function (cardId) {
    setTimeout(function() {
        getcard(cardId , (card)=>{

            if (card.isLocked != null){
                card.isLocked = false;
                card.IsUsed = false;
                card.ForUser = null;

                GiftCards_col.save(card , {safe : true});
            }
        });
    }, TimeOutTime);
};


module.exports = router;