var express = require('express');
var router = express.Router();
var moment = require('moment');
var crypto = require('crypto');
var generator = require('generate-password');
var redis = require("redis"), client = redis.createClient();



// SMS Panel
var Kavenegar = require('kavenegar');
var api = Kavenegar.KavenegarApi({
    apikey: '6A424D32457634757130424C56474A51724161512B773D3D'
});

// MongoDB
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://127.0.0.1:27017/test';

var db;
var Userdb ;

var khoy_r , tabriz_r , urmia_r;

MongoClient.connect(url, function(err, db) {
    Userdb = db.collection('users');

    khoy_r	   = db.collection('khoy_resturant');
    tabriz_r   = db.collection('tariz_resturant');
    urmia_r	   = db.collection('urmia_resturant');
});


senOrderToPhone = function (RestaurantPhone , FaktorNum) {


    api.VerifyLookup({
            token :  FaktorNum,
            receptor:  RestaurantPhone =='09148421808' ? '09149655138' : RestaurantPhone,
            template : 'NewOrderCallPhone'
        },
        function(response, status) {

            console.log('response');
            console.log(response);

            console.log('status');
            console.log(status);

        });
}

router.get('/SenCodeToPhone/:phone', function(req, res, next) { //Sending Verification Code to user Phone via SMS
    try{

        var _phone = req.params.phone;

        var code ;
        Userdb.findOne({phone :_phone } , function(err , doc){
            if (doc ==null){                                                        // User Not Registered
                code = getRand();
                Userdb.insert({phone :_phone  , code : code , SendedCodeDate : moment().format("YYYY-MM-DD") , CodeRequestCount : 1 , authunticted : false , registered : false});

                api.VerifyLookup({
                        token :  code,
                        template : 'ActivationCode',
                        receptor:  _phone

                    },
                    function(response, status) {

                        console.log('response');
                        console.log(response);

                        console.log('status');
                        console.log(status);


                    });
                var s = {};
                s.res           =  200;
                res.end(JSON.stringify(s));
            }
            else{ // Already Registered

                if(doc.authunticted ==true){ // Already authunticted

                    var s = {};
                    s.res = 500;

                    if (doc.registered == true){
                        s.Message = "Already Registered"
                    }else{
                        s.Message = "Already authunticted"
                    }

                    res.end(JSON.stringify(s));

                }else if(doc.SendedCodeDate == moment().format("YYYY-MM-DD")){
                    if (doc.CodeRequestCount <4){							//Send Code By SMS to User
                        code = getRand();
                        doc.code = code;
                        doc.CodeRequestCount = doc.CodeRequestCount+1;

                        api.VerifyLookup({
                                token :  code,
                                template : 'ActivationCode',
                                receptor:  _phone
                            },
                            function(response, status) {

                                console.log('response');
                                console.log(response);

                                console.log('status');
                                console.log(status);


                            });

                        Userdb.save(doc, {safe: true}, function(err) {
                            if (err ==null){
                                var s = {};
                                s.res = 200;
                                res.end(JSON.stringify(s));
                            }
                            else {
                                res.json({res: 500});
                                res.end();
                            }
                        });
                    }
                    else{
                        var s = {};
                        s.res = 500;
                        s.Message = "Limit Reach";

                        res.end(JSON.stringify(s));
                    }
                }else{
                    code = getRand();
                    doc.CodeRequestCount=1;
                    doc.SendedCodeDate = moment().format("YYYY-MM-DD");
                    doc.authunticted = false;
                    doc.code = code;

                    api.VerifyLookup({
                            token :  code,
                            template : 'ActivationCode',
                            receptor:  _phone
                        },
                        function(response, status) {

                            console.log('response');
                            console.log(response);

                            console.log('status');
                            console.log(status);


                        });

                    Userdb.save(doc, {safe: true}, function(err) {
                        if (err ==null){
                            var s = {};
                            s.res = 200;
                            //s.code = code;
                            res.end(JSON.stringify(s));
                        }else {
                            res.json({res: 500});
                            res.end();
                        }
                    });
                }
            }
        });

    } catch(e){
        console.log(e);
        var s = {};
        s.res = "ERR";
        res.end(JSON.stringify(s));
    }
});

router.get('/CheckCode/:phone/:code', function(req, res, next) {
    var s = {};
    try{
        Userdb.findOne({phone :req.params.phone } , function(err , doc){
            if (doc!=null){
                //console.log(doc.code );
                //console.log(req.params.code );

                //console.log(doc.code == req.params.code);

                if (doc.code == req.params.code){

                    doc.authunticted = true;
                    Userdb.save(doc, {safe: true}, function(err) {
                        if (err == null){
                            s.res = 200
                            res.end(JSON.stringify(s));
                        }
                        else{
                            s.res = "Err";
                            res.end(JSON.stringify(s));
                        }
                    });
                }
                else{
                    s.res = 500;
                    s.Message = "Not Match";
                    res.end(JSON.stringify(s));
                }
            }
        });
    } catch(e){
        console.log(e);
    }
});

router.get('/signup/:phone/:fname/:lname/:pass/:email/:address/', function(req, res, next) {

    try{
        var _ = req.params;
        console.log('Email :' + _.email);
        Userdb.findOne({Email : _.email} , function (err , doc){
            if (doc ==null){
                Userdb.findOne( {phone : _.phone} , function(err , doc){

                    var passhash = HashPass(_.pass);

                    doc.FirstName  = _.fname.replace( new RegExp('\\+', 'g'),' ');
                    doc.LastName   = _.lname.replace( new RegExp('\\+', 'g'),' ');
                    if (_.email != "NO+Email")
                        doc.Email = _.email ;

                    doc.Address = [{'Title' : 'آدرس پیش فرض' , value :_.address.replace( new RegExp('\\+', 'g'),' ')}];
                    doc.Password = passhash;
                    doc.registered = true;

                    Userdb.save(doc, {safe: true}, function(err){
                        if(err){
                            var s = {};
                            s.res = 501;
                            s.Message = 'Err';
                            res.end(JSON.stringify(s));
                        }else{
                            var s = {};
                            s.res = 200;
                            s.hash = passhash;
                            res.end(JSON.stringify(s));
                        }
                    });
                });
            }else{
                console.log(doc);
                var s = {};
                s.res = 500;
                s.message = "Email exist";
                res.end(JSON.stringify(s));
            }
        });
    }catch (e){
        console.log(e)
    }
});



router.get('/signup/v2/:phone/:fname/:lname/:pass/', function(req, res, next) {

    try{
        var _ = req.params;

        Userdb.findOne( {phone : _.phone} , function(err , doc){

            var passhash = HashPass(_.pass);

            doc.FirstName  = _.fname.replace( new RegExp('\\+', 'g'),' ');
            doc.LastName   = _.lname.replace( new RegExp('\\+', 'g'),' ');
            
            doc.Password = passhash;
            doc.registered = true;

            Userdb.save(doc, {safe: true}, function(err){
                if(err){
                    var s = {};
                    s.res = 501;
                    s.Message = 'Err';
                    res.end(JSON.stringify(s));
                }else{
                    var s = {};
                    s.res = 200;
                    s.hash = passhash;
                    res.end(JSON.stringify(s));
                }
            });
        });
    }catch (e){
        console.log(e)
    }
});


router.get('/Restaurant_signup/:PhoneNumber/:RestaurantName/:Password/:Email/:Address/:City', function(req, res, next) {

    var _ = req.params;

    var city_rsturant ;

    if (_.City == "khoy"){
        city_rsturant = khoy_r;

    }else if (_.City == "tabriz"){
        city_rsturant = tabriz_r;

    }else if (_.City == "urmia"){
        city_rsturant = urmia_r;

    }else
        res.end('Error')


    var s = {};

    Passhash = HashPass(_.Password);

    if (_.Email == "NO+Email")
        _.Email = "";

    Userdb.findOne( {phone : _.PhoneNumber} , function(err , doc){

        doc.Password = Passhash ;
        doc.restuarant_user =true;
        doc.City = _.City;

        Userdb.save(doc, {safe: true}, function(err) {
            city_rsturant.insert ({ _id : _.PhoneNumber,
                Name : _.RestaurantName.replace( new RegExp('\\+', 'g'),' ') ,
                Address : _.Address.replace( new RegExp('\\+', 'g'),' ') ,
                City : _.City  ,
                Email : _.Email ,
                Active: false ,
                FoodCategories : [{"order" : 0 ,"Name" : "همه"}] } , function (err , result ){

                if(err)
                    s.res = 500;
                else{
                    s.res = 200;
                    s.hash = Passhash;
                }

                res.end(JSON.stringify(s));

            });
        });
    });

});

router.get('/login_Restaurant/:phone/:Pass' , function(req, res, next) {

    console.log('/login_Restaurant :::');

    var _ = req.params;

    console.log(_);

    Userdb.findOne({phone : _.phone} , function (err , doc){
        if (doc != null){

            if( doc.restuarant_user == null) {
                res.json({res: 401});
                res.end();
                return;
            }

            //var hash = HashPass(_.Pass);
            validatePassword ( _.Pass, doc.Password , function( err, result) {

                //console.log(doc.Password  + " **** " + hash + " ********* " + doc.phone , result);

                if (result){

                    var city_rsturant ;

                    if (doc.City == "khoy"){
                        city_rsturant = khoy_r;
                        console.log('login_Restaurant : khoy');
                    }
                    else if (doc.City == "tabriz"){
                        city_rsturant = tabriz_r;
                        console.log('login_Restaurant : tabriz');
                    }
                    else if (doc.City == "urmia"){
                        city_rsturant = urmia_r;
                        console.log('login_Restaurant : urmia');
                    }
                    else
                        res.end('Error');

                    city_rsturant.findOne ( {_id : _.phone} , function (err , restuarant){
                        try {
                            console.log(restuarant);
                            var s 		=  {};
                            s.res 		=  200;
                            s.hash 		=  doc.Password;
                            s.Name 		=  restuarant.Name;
                            s.Address   =  restuarant.Address;
                            s.Email		=  restuarant.Email;
                            s.City 		=  restuarant.City;
                            res.end(JSON.stringify(s));
                        }catch (ex){

                            console.log(ex)
                            res.json({res : 500 , Message : "Password or userName Not Correct"})
                            res.end();
                        }

                    });
                }
                else{
                    var s = {};
                    s.res = 500;
                    s.Message = "Password or userName Not Correct";
                    res.end(JSON.stringify(s));
                }


            });
        }else{
            var s = {};
            s.res = 500;
            s.message = "User Not Exist";
            res.end(JSON.stringify(s));
        }

    });
});

router.get('/login/:phone/:Pass' , function(req, res, next) {
    var _ = req.params;
    Userdb.findOne({phone : _.phone} , function (err , doc){
        if (doc != null){


            if(doc.Password == null ){
                res.json({res : 500 , Message :"Not Registerd Yet" });
                res.end();
            }else {

                var hash = HashPass(_.Pass);
                validatePassword ( _.Pass, doc.Password , function( err, result) {

                    //console.log(doc.Password  + " **** " + hash + " ********* " + doc. phone , result);

                    if (result){
                        var s           =  {};
                        s.res           =  200;
                        s.hash          =  doc.Password ;
                        s.FirstName     =  doc.FirstName;
                        s.LastName      =  doc.LastName;
                        s.Address       =  doc.Address;
                        s.Email         =  doc.Email;

                        res.end(JSON.stringify(s));
                    }
                    else{
                        var s = {};
                        s.res = 500;
                        s.Message = "Password or userName Not Correct";
                        res.end(JSON.stringify(s));
                    }
                });
            }
        }else{
            var s = {};
            s.res = 500;
            s.message = "User Not Exist";
            res.end(JSON.stringify(s));
        }
    });
});

router.get('/change_user_pass/:phone/:CurrentPass/:NewPass' , function(req, res, next) {
    var _ = req.params;
    var s = {};



    Userdb.findOne({phone : _.phone} , function (err , doc){

        if (doc != null){

            var hash = HashPass(_.CurrentPass);
            //console.log("Hash : "+hash);
            //console.log(doc);

            validatePassword ( _.CurrentPass, doc.Password , function( err, result) {

                if (result){

                    var NewPasshash = HashPass(_.NewPass);
                    doc.Password = NewPasshash;

                    Userdb.save(doc, {safe: true}, function(err) {

                        if(err){
                            s.res = 500;
                            s.Message = "Error in saving Pass";
                            res.end(JSON.stringify(s));
                        }
                        else {
                            s.res           =  200;
                            s.hash          =  NewPasshash;
                            res.end(JSON.stringify(s));
                        }
                    });
                }
                else {
                    s.res = 500;
                    s.Message = "Current Password Not Correct";
                    res.end(JSON.stringify(s));
                }
            });
        }else {
            s.res = 500;
            s.message = "User Not Exist";
            res.end(JSON.stringify(s));
        }
    });


});
/*
router.get('/getUserReferralInfo/:Phone/:Pass/', function(req, res, next) {

    var _ = req.params;
    isLogedIn(_.Phone , _.Pass , function (logedIn) {

        if(!logedIn){
            res.json({code : 401})
        }else{

            Userdb.findOne({phone :_.Phone , Password :  _.Pass } , function (err , user) {

                console.log(user);
                if(!err)
                    res.json({res : 200 , ReferralCode : user.ReferralCode , FreeMoney : FreeMoney})
                else
                    res.json({res : 501})

            })
        }
    })
});*/


router.get('/setRestaurantStatuse/:phone/:Pass/:city/:statuse' , function(req, res, next) {
    var _ = req.params;

    isLogedIn(_.phone , _.Pass , function (isLogedIn) {

        if (!isLogedIn){
            res.json({res : 401});
            res.end();
        }else {

            getCity_restaurant(_.city , function (city_rsturant) {

                var isClosed ;
                if (_.statuse == "opend")
                    isClosed = false;
                else
                    isClosed = true;

                city_rsturant.update({_id : _.phone} , {$set : {isClosed :isClosed }} ,function (err , updated) {

                    if(err){
                        res.json({res : 500});
                        res.end();
                    }
                    else {
                        res.json({res : 200});
                        res.end();
                    }
                })
            });
        }

    })

});

router.get('/forgetPass/:phone/' , function(req, res, next) {
    var _ = req.params;
    var userPhoneNumber = _.phone;

    var s = {};

    Userdb.findOne({phone : _.phone} , function (err , doc) {

        if (doc != null) {
            var newPass = GeneratePass();
            var hash = HashPass(newPass);

            doc.Password = hash;

            Userdb.save(doc, {safe: true}, function(err) {

                if(err){
                    s.res = 500;
                    s.Message = "Error in saving Pass";
                    res.end(JSON.stringify(s));
                }else {
                    api.VerifyLookup({
                            token: newPass,
                            receptor: userPhoneNumber ,
                            template : 'ForgetPassword'
                        },
                        function(response, status) {
                            console.log(response);
                            console.log(status);

                            s.res           =  200;
                            res.end(JSON.stringify(s));
                        });
                }
            });
        }else {
            s.res = 500;
            s.Message = "Current Such User";
            res.end(JSON.stringify(s));
        }
    });
});

function isLogedIn(user , pass , callback) {

    Userdb.findOne({phone :user , Password :  pass} , function (err , user) {

        if(err)
            callback(false);
        else {
            callback( user == null ? false : true);
        }
    });
}

var generateSalt = function()
{
    var set = '0123456789aklmnopqurstuvwxyzABCDEFGbcdefghijHIJKLMNOPQURSTUVWXYZ';
    var salt = '';
    for (var i = 0; i < 10; i++) {
        var p = Math.floor(Math.random() * set.length);
        salt += set[p];
    }
    return salt;
}

function md5(str) {
    return crypto.createHash('md5').update(str).digest('hex');
}

function HashPass(pass)
{
    var salt = generateSalt();
    return salt + md5(pass + salt);
}

function validatePassword(plainPass, hashedPass, callback)
{
    var salt = hashedPass.substr(0, 10);
    var validHash = salt + md5(plainPass + salt);
    callback(null, hashedPass === validHash);
}

function GeneratePass(){

    var password = generator.generate({
        length: 10,
        numbers: true
    });

    return password;
}

function getRand() {
    var min=1000, max=9999;
    return Math.floor (Math.random() * (max - min) + min);
}

function getCity_restaurant ( Citystr , callback){

    var city_rsturant ;

    if (Citystr == "khoy"){
        callback(khoy_r);
        console.log('khoy');
    }
    else if (Citystr == "tabriz"){
        callback(tabriz_r);
        console.log('tabriz');
    }
    else if (Citystr == "urmia"){
        callback( urmia_r);
        console.log('urmia');
    }
    else
        callback(null);
}




module.exports = router;