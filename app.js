const express = require('express');
const path = require('path');
//const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const moment = require('moment-timezone');
const index  = require('./routes/index.js');
const users  = require('./routes/users.js');
const orders = require('./routes/orders.js');
const Cards  = require('./routes/Cards.js');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'EJS');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/orders', orders);
app.use('/giftCards', Cards);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let zone = 'Asia/Tehran';
    console.log("Time :: " +  moment.tz(zone).format('YYYY-MM-DD HH:mm:ss') + "\n");
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});


/*
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

	console.log(err)
  // render the error page
  res.status(err.status || 500);
  res.end('Err');
  //res.render('error');
});*/

module.exports = app;

app.listen(8000, function () {
  console.log('Example app listening on port 8000!')
});